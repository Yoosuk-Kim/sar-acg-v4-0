1. Verion : SQLite_v1.1b

2. Requirement : LabVIEW 2023 Q3 or later version

3. Update  History

v1.1b (Working)
(1) File added : readme.txt
(2) Modified : Removed connetion condition 
	Exist where, Update Table, insert Colm_data
(3) Methods created
	Read		: Read Single Cluster, Read 1D Cluster
	Cutom	: Execute Single In64, Execute 1D, Execute NoReturn, Execute Single, Execute 2D
		    	  Execute Single Cluster, Execute 1D Cluster
	Count	: Count Rows
	Calc		: SUM DBL
	JOIN		: LEFT JOIN_2D, LEFT JOIN_1D

v1.0 (Released)
(1) Methods created
	Open/Close	: Close DB, Open DB
	Utilities		: clear PrimaryKey, clear Table, Exist where
	Read/Write		: Update Table, insert Colm_data, Read 1D array, Read 2D array