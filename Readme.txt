SAR Automated Certificate Generator v5.5 (KC Simultaneous Report)
프로그램 실행 후 아래 파일이 자동으로 설치 되지 않는 다면 수동으로 생성하시기 바랍니다. 

단, 3번 파일은 프로그램 실행 후 "Security Model" 항목에 체크해야 생성됨
(프로그램 실행 >> 좌측 상단 KC 선택 >> Security Model 체크)

1. C 드라이브
C:\sqlite\sqlite3.exe

2. 문서
C:\Users\user1\Documents\LabVIEW Data\SQL_DB\KC_SMT.db

3. 보안서버 드라이브(S:)
S:\SQL_DB\KC_SMT.db

* 원본 파일 위치 : 설치 파일(USB)에 위치함
  설치파일\SQL_DB\sqlite3.exe
  설치파일\SQL_DB\KC_SMT.db